<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.stream.Stream"%>

<%@page import="etfbl.aero.dao.ReservationDAO"%>
<%@page import="etfbl.aero.dto.Reservation"%>
<%@page import="java.util.List"%>
<%@page import="etfbl.aero.bean.UserBean"%>
<jsp:include flush="true" page="WEB-INF/header.jsp"/>
<jsp:useBean id="userBean" type="etfbl.aero.bean.UserBean"
	scope="application" />

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Reservations</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">

<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="UserList.js"></script>
<link href="UserList.css" rel="stylesheet" type="text/css">

</head>
<body>

	<div class="container-lg">
		<div class="table">
			<div class="table-wrapper">
				<div class="table-title">
					<div class="row">
						<div class="col-sm-8">
							<h2>
								<b>Resrvations</b>
							</h2>
						</div>

					</div>
				</div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Start location</th>
							<th>Destination</th>
							<th>Date</th>
							<%
								if (userBean.getUser().getUserType().equals("putnicki")) {
							%>
							<th>SeatNumber</th>
							<%
								} else {
							%>
							<th>Load type</th>
							<%
								}
							%>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<%
							List<Reservation> reservations = new ReservationDAO().getAllReservationsForUser(userBean.getUser());

						for (Reservation res : reservations) {
						%><tr>
							<td style="display: none"><%=res.getId()%></td>
							<td><%=res.getStartLocation()%></td>
							<td><%=res.getDestination()%></td>
							<td><%=res.getDate()%></td>
							<%
								if (userBean.getUser().getUserType().equals("putnicki")) {
							%>
							<td><%=res.getSeatNumber()%></td>
							<%
								} else {
							%>
							<td><%=res.getLoadDescription()%></td>
							<%
								}
							%>
							<td><%=res.getStatus()%></td>
							<td><%
								if(res.getStatus().equals("nova")){			
							%>
								<a class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i> </a>
									<%} %></td>
						</tr>
						<%
							}
						%>

					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/reservation.js"></script>
</body>
</html>