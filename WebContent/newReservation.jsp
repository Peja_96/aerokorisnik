<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="etfbl.aero.bean.UserBean"%>
<%@page import="etfbl.aero.bean.AirportBean"%>
<jsp:include flush="true" page="WEB-INF/header.jsp"/>
<jsp:useBean id="userBean" class="etfbl.aero.bean.UserBean"
	scope="application" />
<jsp:useBean id="airportBean" class="etfbl.aero.bean.AirportBean"
	scope="session" />
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Fonts -->
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600"
	rel="stylesheet" type="text/css">



<link rel="icon" href="Favicon.png">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

<title>Reservation</title>
</head>
<body>

	<main class="my-form">
		<div class="cotainer">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">Reserve</div>
						<div class="card-body">
							<form name="my-form" action="reserve?action=addReservation" method="post">
								<div class="form-group row">
									<label for="first_name"
										class="col-md-4 col-form-label text-md-right">Start
										location</label>
									<div class="col-md-3">
										<select class="custom-select" name="country1"
											id="country_select1">
											<%
												for (String country : airportBean.getAllCountries()) {
											%>
											<option value="<%=country%>"><%=country%></option>
											<%
												}
											%>

										</select>
									</div>

									<div class="col-md-3">
										<select class="custom-select" name="city1" id="city_select1">

										</select>
									</div>
								</div>

								<div class="form-group row">
									<label for="country"
										class="col-md-4 col-form-label text-md-right">Destination</label>
									<div class="col-md-3">
										<select class="custom-select" name="country2"
											id="country_select2">
											<%
												for (String country : airportBean.getAllCountries()) {
											%>
											<option value="<%=country%>"><%=country%></option>
											<%
												}
											%>

										</select>
									</div>

									<div class="col-md-3">
										<select class="custom-select" name="city2" id="city_select2">

										</select>
									</div>
								</div>

							

								<div class="form-group row">
									<label for="email_address"
										class="col-md-4 col-form-label text-md-right">Date</label>
									<div class="col-md-6">
										<input type="date" id="date" class="form-control" required
											name="date">
									</div>
								</div>

								<%
									if (userBean.getUser().getUserType().equals("putnicki")) {
								%>
								<div class="form-group row">
									<label for="user_name"
										class="col-md-4 col-form-label text-md-right">Seat
										number</label>
									<div class="col-md-6">
										<input type="number" id="seat_number" class="form-control" required
											name="seat_number">
									</div>
								</div>
								<%
									} else {
								%>
								<div class="form-group row">
									<label for="user_name"
										class="col-md-4 col-form-label text-md-right">Load
										description</label>
									<div class="col-md-6">
										<input type="text" id="load" class="form-control" required
											name="load">
									</div>
								</div>

								<%
									}
								%>
								<div class="form-group row">
									<label for="present_address"
										class="col-md-4 col-form-label text-md-right">Load
										specification</label>
									<div class="col-md-6">
										<input type="file" id="load_file" name="load_file" required class="form-control">
									</div>
								</div>
								<div class="col-md-6 offset-md-4">
									<button type="submit" class="btn btn-primary float-right">
										Book</button>
								</div>
							</form>
						</div>

					</div>
				</div>
			</div>
		</div>

	</main>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="js/city.js"></script>
</body>
</html>