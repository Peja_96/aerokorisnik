<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
	<%@page import="java.util.stream.Stream" %>

		<%@page import="etfbl.aero.dao.FlightDAO" %>
			<%@page import="etfbl.aero.dto.Flight" %>
				
					<%@page import="java.util.List" %>
					<%@page import="etfbl.aero.bean.UserBean"%>
						<jsp:useBean id="userBean" type="etfbl.aero.bean.UserBean" scope="application"/>
						
						<jsp:include flush="true" page="WEB-INF/header.jsp"/>
						<!DOCTYPE html>
						<html>

						<head>
							<meta charset="ISO-8859-1">
							<title>ETFBL_IP_Aero</title>
							<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
								rel="stylesheet">
							
							
						</head>

						<body onload="onload()">
							<%@page import="java.util.*" session ="true" %>
							
							<div class="container-fluid bg-light">
							
								<div class="row row-second">
									<div class="col-2">
										<div style="width: 100%; height: 100%">
											<iframe width="100%" height="100%"
												src="https://maps.google.com/maps?width=100%25&amp;height=100%25&amp;hl=en&amp;q=Banja%20Luka,%20Patre%205+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>
										</div>
									</div>
									<div class="col" >
										<div class="row ">
											<div class="col-6">
												<div class="table-title">
													<div class="row">
														<div class="col-sm-8">
															<h3>
																<b>Outgoing flights</b>
															</h3>
														</div>
													</div>
												</div>
												<table class="table table-striped">
													<thead>
														<tr>
															<th>Destination</th>
															<th>Status</th>
															<th>Time</th>
															<th>Type</th>
														</tr>
													</thead>
													<tbody id="table_body1">
														
													</tbody>
												</table>
											</div>

											<div class="col-6">
												<div class="table-title">
													<div class="row">
														<div class="col-sm-8">
															<h3>
																<b>Arrival flights</b>
															</h3>
														</div>
													</div>
												</div>
												<table class="table table-striped" style="width: 100%;">
													<thead>
														<tr>
															<th>Start location</th>
															<th>Status</th>
															<th>Time</th>
															<th>Type</th>
														</tr>
													</thead>
													<tbody id="table_body2">
														
													</tbody>
												</table>
											</div>
										</div>

									</div>
									<div class="col-2">
										<section class="border p-4 d-flex justify-content-center mb-4">
											<form class="text-center" action="info?action=message" method="post" style="width: 100%;  max-width: 300px">
												<h2>Contact us</h2>

												<!-- Email input -->
												<div class="form-outline mb-4">
													<input type="email" id="email" placeholder="email" name="email"
														class="form-control" required>

												</div>

												<!-- Subject input -->
												<div class="form-outline mb-4">
													<input type="text" id="subject" placeholder="subject" name="subject"
														class="form-control">
												</div>

												<!-- Message input -->
												<div class="form-outline mb-4">
													<textarea class="form-control" id="message" placeholder="message"
														name="message" rows="4" required></textarea>


												</div>

												<!-- Submit button -->
												<input type="submit" value="Send" class="btn btn-primary btn-lg btn-block my-4">
											</form>
										</section>



									</div>
								</div>
								<div class="row row-third bg-secondary" style="position:absolute; width:100%; bottom:0;">
									<h3 class="mb-3 text-center" >ETFBL 2021 - sva prava zadrzana</h3>
								</div>
							</div>

							<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
							<script type="text/javascript" src="js/initial.js"></script>

						</body>

						</html>