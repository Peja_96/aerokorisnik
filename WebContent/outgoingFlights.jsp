<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.stream.Stream"%>

<%@page import="etfbl.aero.dao.FlightDAO"%>
<%@page import="etfbl.aero.dto.Flight"%>
<%@page import="java.util.List"%>

<jsp:include flush="true" page="WEB-INF/header.jsp"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
								rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="js/flights.js"></script>

</head>
<body onload="pripremiFormu('loadOutgoing')">
	
	<div class="container-lg">
		<div >
			<div class="table-wrapper">
				<div class="table-title">
					<div class="row">
						<div class="col-sm-8">
							<h2>
								<b>Outgoing flights</b>
							</h2>
						</div>
					</div>
				</div>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Start location</th>
							<th>Destination</th>
							<th>Status</th>
							<th>Time</th>
							<th>Type</th>
						</tr>
					</thead>
					<tbody id="table_body">
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>