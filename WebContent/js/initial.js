

function onload(){
	loadOutgoing();
	loadArrival();
}

function loadOutgoing(){
	$.ajax({
		type: "POST",
		url: "./info",
		data: {
			action: "loadOutgoing"
		},
		success: function(text) { setTable1(text); },
		dataType: "html",
		cache: false
	});
}

function setTable1(text) {

	const flights = JSON.parse(text);

	let body = document.getElementById('table_body1');
	body.innerHTML = '';

	flights.forEach(flight => {
		let row = document.createElement('tr');
		row.innerHTML = `
            		<td>${flight.destination}</td>
            		<td>${flight.status}</td>
					<td>${flight.time}</td>
					<td>${flight.type}</td>
				`
		body.appendChild(row)
	});

	setTimeout(loadOutgoing,5000)
}

function loadArrival(){
	$.ajax({
		type: "POST",
		url: "./info",
		data: {
			action: "loadArrival"
		},
		success: function(text) { setTable2(text); },
		dataType: "html",
		cache: false
	});
}

function setTable2(text) {

	const flights = JSON.parse(text);

	let body = document.getElementById('table_body2');
	body.innerHTML = '';

	flights.forEach(flight => {
		let row = document.createElement('tr');
		row.innerHTML = `
            		<td>${flight.start_location}</td>
            		<td>${flight.status}</td>
					<td>${flight.time}</td>
					<td>${flight.type}</td>
				`
		body.appendChild(row)
	});

	setTimeout(loadArrival,5000)
}








