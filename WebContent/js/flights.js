

function pripremiFormu(action) {

	$.ajax({
		type: "POST",
		url: "./info",
		data: {
			action: action
		},
		success: function(text) { prikaziGradove(text,action); },
		dataType: "html",
		cache: false
	});

}

function prikaziGradove(text,action) {

	const flights = JSON.parse(text);

	let body = document.getElementById('table_body');
	body.innerHTML = '';

	flights.forEach(flight => {
		let row = document.createElement('tr');
		row.innerHTML = `
					<td>${flight.start_location}</td>
            		<td>${flight.destination}</td>
            		<td>${flight.status}</td>
					<td>${flight.time}</td>
					<td>${flight.type}</td>
				`
		body.appendChild(row)
	});

	setTimeout(pripremiFormu(action),5000)
}

