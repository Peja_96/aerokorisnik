function loadCountries(e) {
    let xhr = new XMLHttpRequest();

    xhr.open('GET', 'https://restcountries.eu/rest/v2/region/europe', true);

    xhr.onload = function(){
        if(this.status === 200){
            
            const customers = JSON.parse(this.responseText);

           
            let output1 = '<option selected>Serbia</option>'
            let number = 1;

            customers.forEach(customer => {
                 
                 output1 += `<option value="${customer.name}">${customer.name}</option>`;
                 number++;
            });
           

          
            document.getElementById('select').innerHTML = output1;
        }
    }

    xhr.send();

}