$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	var actions = $("table td:last-child").html();
	// Append table with add row form on add new button click
    $(".add-new").click(function(){
		$(this).attr("disabled", "disabled");
		var index = $("table tbody tr:last-child").index();
        var row = '<tr>' +
			'<td style="display: none"></td>' +
            '<td><input type="text" class="form-control" name="name" id="name"></td>' +
            '<td><input type="text" class="form-control" name="password" id="password"></td>' +
            '<td><input type="text" class="form-control" name="phone" id="phone"></td>' +
			'<td>' + actions + '</td>' +
        '</tr>';
    	$("table").append(row);		
		$("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });
	// Add row on add button click
	$(document).on("click", ".add", function(){
		
		var empty = false;
		var input = $(this).parents("tr").find('input[type="text"]');
        input.each(function(){
			if(!$(this).val()){
				$(this).addClass("error");
				empty = true;
			} else{
                $(this).removeClass("error");
            }
		});
		$(this).parents("tr").find(".error").first().focus();
		if(!empty){
			input.each(function(){
				$(this).parent("td").html($(this).val());
			});			
			$(this).parents("tr").find(".add, .edit").toggle();
			$(".add-new").removeAttr("disabled");
		}
		var id=$(this).parents('tr')[0].cells[0].innerHTML;
		var username=$(this).parents('tr')[0].cells[1].innerHTML;
		var password=$(this).parents('tr')[0].cells[2].innerHTML;
		var role=$(this).parents('tr')[0].cells[3].innerHTML;
		var method = "edit";
		var role1 = "admin";
		var role2 = "manager";
		var role3 = "customer";
		if(role.localeCompare(role1)== 0 || role.localeCompare(role2) == 0 || role.localeCompare(role3) == 0){
		$.ajax({
					url:'EditUser',
                    type:'POST',
                    data:{
						method : method,
						id: id,
                        username: username,
                        password: password,
                        role: role
                    }
                    
				});
		setTimeout(() => {  location.reload(); }, 500);		
		}else{
			window.alert("Wrong role");
		}
    });
	// Edit row on edit button click
	$(document).on("click", ".edit", function(){
			
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
			$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
		});		
		$(this).parents("tr").find(".add, .edit").toggle();
		$(".add-new").attr("disabled", "disabled");
		
    });
	// Delete row on delete button click
	$(document).on("click", ".delete", function(){
		
        $(this).parents("tr").remove();
		$(".add-new").removeAttr("disabled");
		var method = "delete";
		var id=$(this).parents('tr')[0].cells[0].innerHTML;
		$.ajax({
					url:'EditUser',
                    type:'POST',
                    data:{
						method : method,
						id: id,
                    }
                    
				});	
    });
});