

$(function() {
	$("#country_select1").click(ucitajGradove1);
});

$(function() {
	$("#country_select2").click(ucitajGradove2);
});

function ucitajGradove1(){
	ucitajGradove(1);
}

function ucitajGradove2(){
	ucitajGradove(2);
}
function ucitajGradove(number) {
	let method = 'country_select';
	method += number;
	$.ajax({ 
		type: "POST",
		url: "./info",
		data:{
			action : "loadCities",
			countryName :document.getElementById(method).value
		} ,
		success: function(text) { prikaziGradove(text,number); },
		dataType: "html",
		cache: false
		});
	}
	
function prikaziGradove(text,number){
		
	const customers = JSON.parse(text);

            let method = 'city_select';
			method += number;
            let output = ''
            
            customers.forEach(customer => {
                 
                 output += `<option value="${customer.name}">${customer.name}</option>`;
                
            });

 		document.getElementById(method).innerHTML = output;
}