
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="etfbl.aero.bean.UserBean"%>
						<jsp:useBean id="userBean" type="etfbl.aero.bean.UserBean" scope="application"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
							<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
								rel="stylesheet">
</head>
<body >
<%@page import="java.util.*" session ="true" %>
<div class="container-fluid bg-light">
								<div class="row row-first bg-secondary">

									<div class="col-10" style="height: 100%">
										<div class="row row-second">
											<h1 class="mb-3 text-center p-4"> ETFBL_IP_Aero</h1>
										</div>
										<div class="row">
										
											<div class="col">
												<nav class="navbar navbar-expand-lg navbar-light bg-secondary"
													style="background-color: #e3f2fd;">
													<!-- Container wrapper -->
													<div class="container-fluid">
														<!-- Collapsible wrapper -->
														<div class="collapse navbar-collapse"
															id="navbarSupportedContent">
															
															<ul class="navbar-nav me-auto mb-2 mb-lg-0">
															<li class="nav-item"><a type="button" class="btn btn-outline-primary"
																		href="Controller?action=home" > Home
																		   </a></li>
																<li class="nav-item"><a type="button" class="btn btn-outline-primary"
																		href="Flights?action=outgoingFlights" > Outgoing flights</a></li>
																<li class="nav-item"><a type="button" class="btn btn-outline-primary"
																		href="Flights?action=arrivalFlights" > Arrival flights</a></li>
																<%if (userBean.getUser().getName() != null){ %>		
																<li class="nav-item"><a type="button" class="btn btn-outline-primary"
																		href="Flights?action=newReservation" >New reservation</a></li>
																<li class="nav-item"><a type="button" class="btn btn-outline-primary"
																		href="Flights?action=allResrvations" >All reservation</a>
																</li>
																<%} %>
															</ul>
															<!-- Left links -->
														</div>
														<!-- Collapsible wrapper -->
													</div>
													<!-- Container wrapper -->
												</nav>
											</div>
										</div>

									</div>
									<div class="col-2">
										<section class="p-4 d-flex justify-content-center mb-4">
											<%if (!userBean.isLoggedIn()){ %>
											<form id="form" class="text-center" method="post" action="Controller?action=login" style="width: 100%; max-width: 300px">

												<div class="login">
													<div class="input-group col-lg-6 mb-3">

														<input style="width:200;" id="username" type="text" name="username"
															placeholder="Username"
															class="form-control bg-white mt-1 border-left-0 border-md" required>

													</div>
													<div class="input-group col-lg-6 mb-3">
														<input id="password" type="password" name="password"
															placeholder="Password"
															class="form-control bg-white mt-1 border-left-0 border-md" required>
													</div>
													<div class="col mb-3">
														<a href="?action=registration" 
															class="link-primary float-start">Register</a>
														<input class="btn btn-primary float-end" id="loginBtn"
															type="submit" value="Login">
													</div>

												</div>
											</form>
											<%} else { %>
												<form id="formLogout" class="text-center" method="post" action="Controller?action=logout" style="width: 100%; max-width: 300px">

												<div class="loguot">
													<div class="input-group col-lg-6 mb-3">

														<p class="text-start"><%= userBean.getUser().getName() %></p>

													</div>
													<div class="input-group col-lg-6 mb-3">
														<p class="text-start"><%= userBean.getUser().getSurname() %></p>
													</div>
													<div class="col mb-3">
														
														<input class="btn btn-primary float-end" id="logoutBtn"
															type="submit" value="Logout">
													</div>

												</div>
											</form>
											<%} %>
										</section>
									</div>
								</div>
								</div>
</body>
</html>