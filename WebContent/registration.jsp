<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Fonts -->
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600"
	rel="stylesheet" type="text/css">



<link rel="icon" href="Favicon.png">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link href="css/style.css" rel="stylesheet">
<title>Register</title>
</head>
<body onload="loadCountries()">


	<div class="cotainer-fluid">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Create new account</div>
					<div class="card-body">
						<form name="my-form" action="user" method="post">
							<div class="form-group row">
								<label for="name" class="col-md-4 col-form-label text-md-right">First
									name</label>
								<div class="col-md-6">
									<input type="text" id="name" class="form-control" name="name"
										required>
								</div>
							</div>

							<div class="form-group row">
								<label for="surname"
									class="col-md-4 col-form-label text-md-right">Last name</label>
								<div class="col-md-6">
									<input type="text" id="surname" name="surname"
										class="form-control" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="email" class="col-md-4 col-form-label text-md-right">E-Mail
									Address</label>
								<div class="col-md-6">
									<input type="text" id="email" class="form-control" name="email"
										required>
								</div>
								
							</div>
							<div class="form-group row">
							<div><%=session.getAttribute("email")!=null?session.getAttribute("email").toString():""%></div>
							</div>
							<div class="form-group row">
								<label for="username"
									class="col-md-4 col-form-label text-md-right">User Name</label>
								<div class="col-md-6">
									<input type="text" id="username" class="form-control"
										name="username" required>
								</div>
							</div>



							<div class="form-group row">
								<label for="address"
									class="col-md-4 col-form-label text-md-right">Present
									Address</label>
								<div class="col-md-6">
									<input type="text" id="address" name="address"
										class="form-control" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password"
									class="col-md-4 col-form-label text-md-right">Password</label>
								<div class="col-md-6">
									<input id="password" type="password" name="password" required
										class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="repeatPassword"
									class="col-md-4 col-form-label text-md-right">Repeat
									password</label>
								<div class="col-md-6">
									<input id="repeatPassword" type="password" name="repeatPassword" required
										class="form-control">
								</div>
							</div>
							<div class="form-group row">
							<div><%=session.getAttribute("password")!=null?session.getAttribute("password").toString():""%></div>
							</div>
							<div class="form-group row">
								<label for="userType"
									class="col-md-4 col-form-label text-md-right">User type</label>
								<div class="col-md-6">
									<div class="form-check">
										<input class="form-check-input" type="radio" value="option1"
											name="flexRadioDefault" id="flexRadioDefault1" checked> <label
											class="form-check-label" for="flexRadioDefault1">
											transportni </label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="radio" value="option2"
											name="flexRadioDefault" id="flexRadioDefault2">
										<label class="form-check-label" for="flexRadioDefault2">
											putnicki </label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label for="country"
									class="col-md-4 col-form-label text-md-right">Country</label>
								<div class="col-md-3">
									<select class="custom-select" name="country" id="select">
									</select>
								</div>
							</div>
							<div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-primary float-right">
									Register</button>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>



	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="js/script1.js"></script>
</body>
</html>