package etfbl.aero.dto;

import java.sql.Timestamp;

public class Flight {
	
	private String startLocation;
	private String destination;
	private String status;
	private String type;
	private Timestamp time;
	
	
	public Flight(String startLocation, String destination, String status, String type, Timestamp time) {
		super();
		this.startLocation = startLocation;
		this.destination = destination;
		this.status = status;
		this.type = type;
		this.time = time;
	}
	public String getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Timestamp getTime() {
		return time;
	}
	public void setTime(Timestamp time) {
		this.time = time;
	}
}
