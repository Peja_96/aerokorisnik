package etfbl.aero.dto;

import java.sql.Date;


public class Reservation {
	private Integer id;
	private String startLocation;
	private String destination;
	private String loadDescription;
	private String specificationFile;
	private String status;
	private String reason;
	private Integer seatNumber;
	private Date date;
	

	public Reservation(String startLocation, String destination, String loadDescription, String specificationFile,
			String status, String reason, Integer seatNumber, Date date) {
		super();
		this.startLocation = startLocation;
		this.destination = destination;
		this.loadDescription = loadDescription;
		this.specificationFile = specificationFile;
		this.status = status;
		this.reason = reason;
		this.seatNumber = seatNumber;
		this.date = date;
	}
	
	public Reservation(String startLocation, String destination,
			String status, Date date, Integer id) {
		super();
		this.startLocation = startLocation;
		this.destination = destination;
		this.status = status;
		this.date = date;
		this.id = id;
	}
	
	public Reservation() {}
	
	public String getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getLoadDescription() {
		return loadDescription;
	}
	public void setLoadDescription(String loadDescription) {
		this.loadDescription = loadDescription;
	}
	public String getSpecificationFile() {
		return specificationFile;
	}
	public void setSpecificationFile(String specificationFile) {
		this.specificationFile = specificationFile;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(Integer seatNumber) {
		this.seatNumber = seatNumber;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
}
