package etfbl.aero.dto;

public class Airport {
	private String countryName;
	private String cityName;
	public Airport(String countryName, String cityName) {
		super();
		this.countryName = countryName;
		this.cityName = cityName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	
}
