package etfbl.aero.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import etfbl.aero.bean.UserBean;

@WebServlet("/Controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		ServletContext servletContext = request.getServletContext();
		UserBean userBean;
		
		String address = "/index.jsp";
		String action = request.getParameter("action");
		HttpSession session = request.getSession();
		
		
		
		session.setAttribute("notification", "");
		if (action == null || action.equals("")) {
			userBean = new UserBean();
			address = "index.jsp";
			servletContext.setAttribute("userBean", userBean);
		} else if (action.equals("logout")) {
			session.invalidate();
			servletContext.setAttribute("userBean", new UserBean());
		} else if (action.equals("login")) {
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			userBean = new UserBean();
			if (userBean.login(username, password)) {
				servletContext.setAttribute("userBean", userBean);
				session.setAttribute("name", userBean.getUser().getName());
				session.setAttribute("surname", userBean.getUser().getSurname());
				session.setAttribute("userType", userBean.getUser().getUserType());
			} else {
				session.setAttribute("notification", "Pogresni parametri za pristup");
			}
		}
		else if(action.equals("registration")) {
			address = "registration.jsp";
			
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(address);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
