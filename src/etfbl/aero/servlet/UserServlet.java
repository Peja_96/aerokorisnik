package etfbl.aero.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import etfbl.aero.bean.UserBean;
import etfbl.aero.dao.UserDAO;
import etfbl.aero.dto.User;

/**
 * Servlet implementation class User
 */
@WebServlet("/user")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		session.removeAttribute("email");
		session.removeAttribute("password");
		ServletContext servletContext = request.getServletContext();
		String address = "/registration.jsp";
		UserBean userBean;
		if(UserDAO.containsEmail(request.getParameter("email")))
			session.setAttribute("email", "E-mail je zauzet");
		else if(!request.getParameter("password").equals(request.getParameter("repeatPassword"))) {
			
			session.setAttribute("password", "Pogresna sifra");
		}
		else {
			
			UserDAO.addUser(new User(request.getParameter("name"), request.getParameter("surname"), request.getParameter("username"),
					request.getParameter("password"), request.getParameter("email"), request.getParameter("flexRadioDefault").equals("option1")? "transportni":"putnicki",
					request.getParameter("country"), request.getParameter("address")));
			userBean = new UserBean();
			userBean.login(request.getParameter("username"), request.getParameter("password"));
			servletContext.setAttribute("userBean", userBean);
			session.setAttribute("name", userBean.getUser().getName());
			session.setAttribute("surname", userBean.getUser().getSurname());
			session.setAttribute("userType", userBean.getUser().getUserType());
			address = "/index.jsp";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(address);
		dispatcher.forward(request, response);
		//response.sendRedirect("index.jsp");
	}

}
