package etfbl.aero.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import etfbl.aero.dao.AirportDAO;
import etfbl.aero.dao.FlightDAO;
import etfbl.aero.dao.MessageDAO;
import etfbl.aero.dto.Airport;
import etfbl.aero.dto.Flight;

/**
 * Servlet implementation class Airports
 */
@WebServlet("/info")
public class InfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InfoServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("action").equals("loadCities")) {
			String countryName = request.getParameter("countryName");
			PrintWriter pw = response.getWriter();

			JSONArray ja = new JSONArray();

			for (String airport : AirportDAO.getAllCities(countryName)) {
				JSONObject jo = new JSONObject();
				jo.put("name", airport);
				ja.put(jo);
			}

			pw.println(ja);

			pw.close();
		} else if (request.getParameter("action").equals("loadArrival")) {
			PrintWriter pw = response.getWriter();

			JSONArray ja = new JSONArray();

			for (Flight flight : new FlightDAO().getAllArrivalFlights()) {
				JSONObject jo = new JSONObject();
				jo.put("start_location", flight.getStartLocation());
				jo.put("destination", flight.getDestination());
				jo.put("time", flight.getTime());
				jo.put("status", flight.getStatus());
				jo.put("type", flight.getType());
				ja.put(jo);
			}

			pw.println(ja);

			pw.close();
		} else if (request.getParameter("action").equals("loadOutgoing")) {
			PrintWriter pw = response.getWriter();

			JSONArray ja = new JSONArray();

			for (Flight flight : new FlightDAO().getAllOutgoingFlights()) {
				JSONObject jo = new JSONObject();
				jo.put("start_location", flight.getStartLocation());
				jo.put("destination", flight.getDestination());
				jo.put("time", flight.getTime());
				jo.put("status", flight.getStatus());
				jo.put("type", flight.getType());
				ja.put(jo);
			}

			pw.println(ja);

			pw.close();
		} else if (request.getParameter("action").equals("message")) {
			String email = request.getParameter("email");
			String subject = request.getParameter("subject");
			String message = request.getParameter("message");
			new MessageDAO().addMessage(email, subject, message);
			RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
			dispatcher.forward(request, response);
		}
		
	}

}
