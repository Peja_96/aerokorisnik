package etfbl.aero.servlet;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import etfbl.aero.bean.UserBean;
import etfbl.aero.dao.ReservationDAO;
import etfbl.aero.dto.Reservation;

/**
 * Servlet implementation class ReservationsServlet
 */
@WebServlet("/reserve")
public class ReservationsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReservationsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext servletContext = request.getServletContext();
		
		String action = request.getParameter("action");
		UserBean userBean = (UserBean)servletContext.getAttribute("userBean");
		if(action.equals("addReservation"))
		{
			Reservation reservation = new Reservation();
			reservation.setDate(java.sql.Date.valueOf(request.getParameter("date")));
			reservation.setStartLocation(request.getParameter("country1") + " " + request.getParameter("city1"));
			reservation.setDestination(request.getParameter("country2") + " " + request.getParameter("city2"));
			reservation.setSpecificationFile(request.getParameter("load_file"));
			if(userBean.getUser().getUserType().equals("putnicki")) {
				reservation.setSeatNumber(Integer.parseInt(request.getParameter("seat_number")));
				reservation.setLoadDescription(null);
			}
			else {
				reservation.setSeatNumber(null);
				reservation.setLoadDescription(request.getParameter("load"));
			}
			new ReservationDAO().addReservation(reservation, userBean.getUser().getId());
		}
		else if(action.equals("delete")) {
			new ReservationDAO().delete(Integer.parseInt(request.getParameter("id")));
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/reservationList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
