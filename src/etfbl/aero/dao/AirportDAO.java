package etfbl.aero.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import etfbl.aero.DB.DBConnection;
import etfbl.aero.dto.Airport;

public class AirportDAO {
	public List<Airport> getAllFlights() {
		List<Airport> airports = new LinkedList<>();

		try {

			Statement statement = DBConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from airport");
			while (resultSet.next()) {
				
				String countryName = resultSet.getString("country_name");
				String cityName = resultSet.getString("city_name");
				
				airports.add(new Airport(countryName, cityName));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return airports;
	}
	
	public static List<String> getAllCountries(){
		List<String> countries = new LinkedList<>();

		try {

			Statement statement = DBConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select country_name from airport");
			while (resultSet.next()) {
								
				countries.add(resultSet.getString("country_name"));
				
			}
			Set<String> set = new HashSet<>(countries);
			countries.clear();
			countries.addAll(set);
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return countries;
	
	}
	
	public static List<String> getAllCities(String country){
		List<String> cities = new LinkedList<>();

		try {
			
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con
					.prepareStatement("Select * from airport where country_name = ?");
			ps.setString(1, country);
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
								
				cities.add(resultSet.getString("city_name"));
				
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return cities;
	
	}
}
