package etfbl.aero.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import etfbl.aero.DB.DBConnection;
import etfbl.aero.dto.Reservation;
import etfbl.aero.dto.User;

public class ReservationDAO {
	public List<Reservation> getAllReservations() {
		List<Reservation> reservations = new LinkedList<>();

		try {

			Statement statement = DBConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from reservation");
			while (resultSet.next()) {
			

				String startLocation = resultSet.getString("start_location");
				String destination = resultSet.getString("destination");
				String status = resultSet.getString("status");
				String loadDescription = resultSet.getString("load_description");
				String specificationFile = resultSet.getString("specification_file");
				String reason = resultSet.getString("reason");
				Integer seatNumber = resultSet.getInt("seat_number");
				Date date = resultSet.getDate("date");
				reservations.add(new Reservation(startLocation, destination, loadDescription,  specificationFile,
						 status, reason,  seatNumber,  date));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reservations;
	}
	
	public List<Reservation> getAllReservationsForUser(User user) {
		List<Reservation> reservations = new LinkedList<>();

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con
					.prepareStatement("Select * from reservation where user_id = ?");
			ps.setInt(1, user.getId());
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
				Reservation reservation = new Reservation(resultSet.getString("start_location"),resultSet.getString("destination"),
						resultSet.getString("status"),resultSet.getDate("date"),resultSet.getInt("id"));

				if(user.getUserType().equals("putnicki")) {
					Integer seatNumber = resultSet.getInt("seat_number");
					reservation.setSeatNumber(seatNumber);
				}else {
					String loadDescription = resultSet.getString("load_description");
					reservation.setLoadDescription(loadDescription);
				}

				reservations.add(reservation);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reservations;
	}
	
	public void addReservation(Reservation reservation, int userID) {
		try {
			String sql;
			if(reservation.getSeatNumber() == null)
				sql = "insert into reservation values(null, ?, ?, ?, null, ?, ?, ?, ?,?)";
			else
				sql ="insert into reservation values(null, ?, ?, ?, ?, null, ?, ?, ?,?)";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);

			statement.setDate(1, reservation.getDate());
			statement.setString(2, reservation.getStartLocation());
			statement.setString(3, reservation.getDestination());
			if(reservation.getSeatNumber() == null)
				statement.setString(4, reservation.getLoadDescription());
			else
				statement.setInt(4, reservation.getSeatNumber());
			
			statement.setString(5, reservation.getSpecificationFile());
			statement.setString(6, "nova");
			statement.setString(7, "");
			statement.setInt(8, userID);
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			
		}
	}
	
	public void delete(int id) {
		try {
			String sql = "delete from reservation where id = ?";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
	}
}
