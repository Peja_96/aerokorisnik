package etfbl.aero.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import etfbl.aero.DB.DBConnection;

public class MessageDAO {

	public void addMessage(String email, String subject, String message) {
		try {
			String sql;
				sql ="insert into message values(null, ?, ?, ?, ?)";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);

			statement.setString(1, email);
			statement.setString(2, subject);
			statement.setString(3, message);
			statement.setInt(4, 0);
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			
		}
	}
}
