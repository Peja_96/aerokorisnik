package etfbl.aero.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import etfbl.aero.DB.DBConnection;
import etfbl.aero.dto.Flight;

public class FlightDAO {
	public List<Flight> getAllFlights() {
		List<Flight> flights = new LinkedList<>();

		try {

			Statement statement = DBConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from flight");
			while (resultSet.next()) {
				// int id = resultSet.getInt("ID");

				String startLocation = resultSet.getString("start_location");
				String destination = resultSet.getString("destination");
				String status = resultSet.getString("status");
				String type = resultSet.getString("type");
				Timestamp time = resultSet.getTimestamp("time");
				flights.add(new Flight(startLocation, destination, status, type, time));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return flights;
	}

	public List<Flight> getAllOutgoingFlights() {
		List<Flight> flights = new LinkedList<>();

		try {

			Statement statement = DBConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from flight where start_location='banja luka'");
			while (resultSet.next()) {
				// int id = resultSet.getInt("ID");

				String startLocation = resultSet.getString("start_location");
				String destination = resultSet.getString("destination");
				String status = resultSet.getString("status");
				String type = resultSet.getString("type");
				Timestamp time = resultSet.getTimestamp("time");
				flights.add(new Flight(startLocation, destination, status, type, time));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return flights;
	}
	
	public List<Flight> getAllArrivalFlights() {
		List<Flight> flights = new LinkedList<>();

		try {

			Statement statement = DBConnection.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from flight where destination='banja luka'");
			while (resultSet.next()) {
				// int id = resultSet.getInt("ID");

				String startLocation = resultSet.getString("start_location");
				String destination = resultSet.getString("destination");
				String status = resultSet.getString("status");
				String type = resultSet.getString("type");
				Timestamp time = resultSet.getTimestamp("time");
				flights.add(new Flight(startLocation, destination, status, type, time));
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return flights;
	}
}
