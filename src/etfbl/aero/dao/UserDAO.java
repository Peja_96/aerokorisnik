package etfbl.aero.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import etfbl.aero.DB.DBConnection;
import etfbl.aero.dto.User;

public class UserDAO {
	public static User validate(String username, String password)  {

		User user = null;
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con
					.prepareStatement("Select * from user inner join person where username = ? and password = ?");
			ps.setString(1, username);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				user = new User();
				user.setUsername(username);
				user.setPassword(password);
				user.setName(rs.getString("name"));
				user.setSurname(rs.getString("surname"));
				user.setUserType(rs.getString("user_type"));
				user.setId(rs.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return user;
	}

	public static boolean addUser(User user) {
		Integer id = 0;
		try {
			String sql = "insert into person values(null, ?, ?, ?, ?)";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, user.getName());
			statement.setString(2, user.getSurname());
			statement.setString(3, user.getUsername());
			statement.setString(4, user.getPassword());
			statement.executeUpdate();

			ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next())
				id = resultSet.getInt(1);
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		try {
			String sql = "insert into user values(?, ?, ?, ?, ?)";
			PreparedStatement statement = DBConnection.getConnection().prepareStatement(sql);
			statement.setInt(1, id);
			statement.setString(2, user.getAddress());
			statement.setString(3, user.getUserType());
			statement.setString(4, user.getEmail());
			statement.setString(5, user.getCountry());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static boolean containsEmail(String email) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("Select * from user inner join person where email = ?");
			ps.setString(1, email);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static void addLogin() {
		try {
			Date date = new Date(new java.util.Date().getTime());
			String sql = "insert into logged values(?, ?) on duplicate key update counter = counter +1";
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setDate(1, date);
			ps.setInt(2, 1);
			ps.executeUpdate();

			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
