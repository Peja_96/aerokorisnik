package etfbl.aero.util;

import java.io.IOException;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.tomcat.util.json.JSONParser;

public class JsonParser {
	public void readFromUrl() {
		 try {
			URL url = new URL("https://restcountries.eu/rest/v2/region/europe");
			HttpURLConnection conn = (HttpURLConnection)url.openConnection(); 
			conn.setRequestMethod("GET"); 
			conn.connect();
			
			
			
			int responsecode = conn.getResponseCode();
			if(responsecode != 200)
				throw new RuntimeException("HttpResponseCode:"  +responsecode);
				else
				{
					Scanner sc = new Scanner(url.openStream());
					String inline = "";
					while(sc.hasNext())
					{
					inline+=sc.nextLine();
					}
					
					JSONArray array = new JSONArray(inline);
					for (int i = 0; i < array.length(); i++) {
						JSONObject row =  array.getJSONObject(i);
						System.out.println(row.getString("name") + " " + row.getString("capital"));
					}
					    
					sc.close();
				}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
