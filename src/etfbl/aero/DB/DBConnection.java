package etfbl.aero.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
private static Connection connection;
	
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public static Connection getConnection() throws SQLException {
		if (connection == null)
			return connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ip?useSSL=false&serverTimezone=UTC", "root", "elektro8");
		return connection;
	}


	/*public Connection getConnection() {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Sigurnost?useSSL=false&serverTimezone=UTC", "root", "elektro8");
			return con;
		}
		catch(Exception x) {
			return null;
		}
		
	}*/
}
