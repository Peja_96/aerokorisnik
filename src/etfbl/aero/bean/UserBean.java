package etfbl.aero.bean;

import etfbl.aero.dao.UserDAO;
import etfbl.aero.dto.User;


public class UserBean {

	private User user = new User();
	private boolean isLoggedIn = false;

	public boolean login(String username, String password) {
		if ((user = UserDAO.validate(username, password)) != null) {
			isLoggedIn = true;
			UserDAO.addLogin();
			return true;
		}
		return false;
	}

	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	public void logout() {
		user = new User();
		isLoggedIn = false;
	}

	public User getUser() {
		return user;
	}

	public boolean isUserNameAllowed(String username) {
		return UserDAO.containsEmail(username);
	}
	
	public boolean add(User user) {
		return UserDAO.addUser(user);
	}
}
