package etfbl.aero.bean;

import java.util.List;

import etfbl.aero.dao.AirportDAO;

public class AirportBean {
	
	
	
	public List<String> getAllCountries(){
		return AirportDAO.getAllCountries();
	}
	
	public List<String> getAllCities(String country){
		return AirportDAO.getAllCities(country);
	}
	
}
